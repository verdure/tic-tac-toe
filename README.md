# Tic-Tac-Toe

A basic Tic-Tac-Toe game. Two players compete against each other to be the 
first to fill a row, column, or diagonal pattern with their marker (either 'X'
or 'O').

This program constantly checks whether the board is full, whether there is a 
winner, and whether there is a tie.